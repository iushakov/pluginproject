// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Factorial.h"
#include "FactorialBPLibrary.h"

UFactorialBPLibrary::UFactorialBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}

int UFactorialBPLibrary::FactorialFunction(int Parameter)
{
	UE_LOG(LogTemp, Log, TEXT("Parameter: %d"), Parameter);

	if (Parameter < 0)
	{
		return Parameter;
	}

	int Result = 1;
	while (Parameter != 0)
	{
		Result *= Parameter--;
	}

	return Result;
}

